using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    // the cameras position should be the same as the car

    // Creates a game object which we can attach to our player controlled car
    [SerializeField] GameObject thingToFollow;

    void LateUpdate()
    {
        // makes the camera follow the car!
        // the Vector3 tells the camera to back up 10 Units so it's not directly on top of the car
        transform.position = thingToFollow.transform.position + new Vector3(0, 0, -10);
    }
}
